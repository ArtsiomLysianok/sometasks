import com.itechart.cities.entity.Edge;
import com.itechart.cities.entity.Vertex;
import com.itechart.cities.util.DijkstraUtil;
import com.itechart.cities.util.DijkstraUtilImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class DijkstraUtilImplTest {

    private DijkstraUtil dijkstraUtil = new DijkstraUtilImpl();
    private Vertex vertexA = new Vertex("Vertex-A");

    @Before
    public void setUp() throws Exception {
        Vertex vertexB = new Vertex("Vertex-B");
        Vertex vertexC = new Vertex("Vertex-C");
        Vertex vertexD = new Vertex("Vertex-D");
        Vertex vertexE = new Vertex("Vertex-E");

        vertexA.addNeighbour(new Edge(11, vertexA, vertexC));
        vertexA.addNeighbour(new Edge(24, vertexA, vertexB));
        vertexC.addNeighbour(new Edge(14, vertexC, vertexB));
        vertexC.addNeighbour(new Edge(9, vertexC, vertexD));
        vertexC.addNeighbour(new Edge(3, vertexC, vertexE));
        vertexB.addNeighbour(new Edge(7, vertexB, vertexD));
        vertexD.addNeighbour(new Edge(6, vertexD, vertexE));
    }

    @Test
    public void getAchievableVertexes() {
        List<Vertex> result = dijkstraUtil.getAchievableVertexes(vertexA, 21);
        Assert.assertEquals(3, result.size());
        result = dijkstraUtil.getAchievableVertexes(vertexA, 12);
        Assert.assertEquals(1, result.size());
        result = dijkstraUtil.getAchievableVertexes(vertexA, 31);
        Assert.assertEquals(4, result.size());
    }
}