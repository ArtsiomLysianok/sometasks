package com.itechart.cities.util;

import com.itechart.cities.entity.Edge;
import com.itechart.cities.entity.Vertex;

import java.util.*;

public class DijkstraUtilImpl implements DijkstraUtil {
    private Map<Vertex, AdditionalData> achievableVertexes = new HashMap<>();
    private Set<Vertex> visitedVertexes = new HashSet<>();
    private List<Vertex> queue = new LinkedList<>();

    @Override
    public List<Vertex> getAchievableVertexes(Vertex sourceVertex, int maxBoundary) {
        findPaths(sourceVertex, maxBoundary);
        List<Vertex> result = new ArrayList<>(achievableVertexes.keySet());
        result.remove(sourceVertex);
        return result;
    }

    private void findPaths(Vertex sourceVertex, int maxBoundary) {
        achievableVertexes.clear();
        visitedVertexes.clear();
        achievableVertexes.put(sourceVertex, new AdditionalData());
        achievableVertexes.get(sourceVertex).setDistance(0);
        queue.add(sourceVertex);
        visitedVertexes.add(sourceVertex);
        AdditionalData targetData, actualData;
        Vertex actualVertex;

        while (!queue.isEmpty()) {
            actualVertex = getVertexWithMinDistance();
            actualData = getAdditionalData(actualVertex);

            for (Edge edge : actualVertex.getAdjacenciesList()) {

                Vertex targetVertex = edge.getTargetVertex();

                if (!visitedVertexes.contains(targetVertex)) {

                    double newDistance = actualData.getDistance() + edge.getWeight();
                    targetData = getAdditionalData(targetVertex);

                    if (newDistance < targetData.getDistance() && newDistance < maxBoundary) {
                        if (!achievableVertexes.containsKey(targetVertex)) {
                            achievableVertexes.put(targetVertex, new AdditionalData());
                        }
                        queue.remove(targetVertex);
                        targetData = achievableVertexes.get(targetVertex);
                        targetData.setDistance(newDistance);
                        targetData.setPredecessor(actualVertex);
                        queue.add(targetVertex);
                    }
                }
            }
            visitedVertexes.add(actualVertex);
        }
    }

    private AdditionalData getAdditionalData(Vertex vertex) {
        AdditionalData data;
        if (achievableVertexes.containsKey(vertex)) {
            data = achievableVertexes.get(vertex);
        } else {
            data = new AdditionalData();
        }
        return data;
    }

    private Vertex getVertexWithMinDistance() {
        if (queue.size() < 1) return null;
        double minValue = Double.MAX_VALUE;
        Vertex target = null;
        AdditionalData additionalData;
        for (Vertex vertex : queue) {
            additionalData = getAdditionalData(vertex);
            if (additionalData.getDistance() < minValue) {
                target = vertex;
                minValue = additionalData.getDistance();
            }
        }
        queue.remove(target);
        return target;
    }

    private class AdditionalData {
        private Vertex predecessor;
        private double distance = Double.MAX_VALUE;

        public Vertex getPredecessor() {
            return predecessor;
        }

        public void setPredecessor(Vertex predecessor) {
            this.predecessor = predecessor;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }
    }
}
