package com.itechart.cities.util;

import com.itechart.cities.entity.Vertex;

import java.util.List;

public interface DijkstraUtil {
    List<Vertex> getAchievableVertexes(Vertex startPoint, int upperBoundary);
}
