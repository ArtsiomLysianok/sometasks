package com.itechart.cities.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Vertex {

    private String name;
    private List<Edge> adjacenciesList;

    public Vertex(String name) {
        this.name = name;
        this.adjacenciesList = new ArrayList<>();
    }

    public void addNeighbour(Edge edge) {
        this.adjacenciesList.add(edge);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Edge> getAdjacenciesList() {
        return adjacenciesList;
    }

    public void setAdjacenciesList(List<Edge> adjacenciesList) {
        this.adjacenciesList = adjacenciesList;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return Objects.equals(name, vertex.name) &&
                Objects.equals(adjacenciesList, vertex.adjacenciesList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, adjacenciesList);
    }
}