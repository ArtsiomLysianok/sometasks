package com.itechart.depbuilder.entity;

import java.util.ArrayList;
import java.util.List;

public class Library {
    private String name;
    private List<Library> dependencies = new ArrayList<>();

    public Library(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Library> getDependencies() {
        return dependencies;
    }

    public void addDependency(Library dependency) {
        this.dependencies.add(dependency);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Library library = (Library) o;

        if (!name.equals(library.name)) return false;
        return dependencies != null ? dependencies.equals(library.dependencies) : library.dependencies == null;
    }

    @Override
    public int hashCode() {
        int result = 31 * name.hashCode();
        result = 31 * result + (dependencies != null ? dependencies.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Library{" +
                "name='" + name + '\'' +
                '}';
    }
}
