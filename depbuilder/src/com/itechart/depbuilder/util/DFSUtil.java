package com.itechart.depbuilder.util;

import com.itechart.depbuilder.entity.Library;

import java.util.List;

public interface DFSUtil {
    List<Library> getAllDependencies(Library root);
}
