package com.itechart.depbuilder.util;

import com.itechart.depbuilder.entity.Library;

import java.util.*;

public class DFSUtilImpl implements DFSUtil {
    private List<Library> visitedLibs = new ArrayList<>();
    private Deque<Library> queue = new LinkedList<>();

    @Override
    public List<Library> getAllDependencies(Library root) {
        queue.addAll(root.getDependencies());
        Library actualDependency;
        Library slave;
        while (!queue.isEmpty()) {
            actualDependency = queue.pollLast();
            if (!visitedLibs.contains(actualDependency)) {
                slave = actualDependency;
                queue.addAll(slave.getDependencies());
                visitedLibs.add(slave);
            }
        }
        return visitedLibs;
    }
}
