package com.itechart.depbuilder;


import com.itechart.depbuilder.entity.Library;
import com.itechart.depbuilder.util.DFSUtil;
import com.itechart.depbuilder.util.DFSUtilImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class DFSUtilImplTest {

    private DFSUtil dfsUtil = new DFSUtilImpl();
    private Library rootLibrary;
    private Library springCore = new Library("spring-core");

    @Before
    public void setUp() throws Exception {
        Library springBeans = new Library("spring-beans");
        Library springContext = new Library("spring-context");
        Library springBoot = new Library("spring-boot");
        Library springData = new Library("spring-data");
        rootLibrary = new Library("myapp");

        springContext.addDependency(springCore);
        springContext.addDependency(springBeans);
        springBeans.addDependency(springCore);
        //springBoot.addDependency(springCore);
        springBoot.addDependency(springContext);

        rootLibrary.addDependency(springBoot);
        rootLibrary.addDependency(springData);
    }

    @Test
    public void getAllDependenciesSuccessWithLibraryDuplication() {
        rootLibrary.getDependencies().stream()
                .filter((library) -> library.getName().equals("spring-boot"))
                .findFirst().ifPresent((library) -> library.addDependency(springCore));
        List<Library> result = dfsUtil.getAllDependencies(rootLibrary);
        Assert.assertEquals(result.size(), 5);
    }

    @Test
    public void getAllDependenciesSuccess() {
        List<Library> result = dfsUtil.getAllDependencies(rootLibrary);
        Assert.assertEquals(result.size(), 5);
    }
}